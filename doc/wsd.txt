title monsv Example Usage

MainApp->Client: initialize
activate MainApp

Client->Registry: publish object
note over Client: I'm done ;)

MainApp->Publisher: start publishing
activate Publisher
note over MainApp: goes on\n working

loop while not stopped
note over Publisher:
    wait till 
    next update
end note

Publisher->Registry: give me all objects
loop for all objects
Registry->Publisher: here is one
note over Publisher:
   publish the object
end note
end
end loop

MainApp->Publisher: stop publishing
destroy Publisher

Client->Registry: remove object
/**
@mainpage  monsvc 
@authors rhauser, ctalau

@section MonSvcIntro Introduction

This package provides a generic and stand-alone interface for monitoring objects. 

@section MonSvcOverview Overview
  The interface consists of two main components:
  - the monsvc::MonitoringService singleton which is used by the application to 
    register objects to be monitored under a name.
  - the monsvc::PublishingController used to configure and start/stop the publishing 
    of monitored objects.

  It supports publishing of histograms, ISInfo objects as well as simple enough
  TObject objects.

  @todo see the documentation of the class for more details about what simple means.

  The library supports multiple publishing destinations like: IS servers,
  OH servers, output streams and ROOT files.

@section MonSvcConfiguration Configuration

@subsection MonSvcConfigurationOverview Overview
  
  The publishing process is governed by a set of monsvc::ConfigurationRule. Each
  rule specifies by using a monsvc::NameFilter, to which of the registered objects it
  applies. The rule also contains a publishing parameters which are specific 
  to the publishing destination:
  - IS server: IS server name, publishing interval
  - OH server: IS server name, OH ROOT provider name, publishing interval
  - ROOT File: File name
  - Output Stream: The output stream.
  
  If two rules match one object, the object is published according to
  each of them. If two rules have the same name, the one which was added later
  by the configuration overwrites the previous one. 

@subsection MonSvcConfigurationOKS OKS Schema 

  The configuration can be done through OKS, by using the following schema 
  (installed as 'daq/schema/monsvc_config.schema.xml').

  @image html schema-view.png 

  As it can be seen from the schema, an application that wants to configure the
  library with OKS should extend PublishingApplication. This will allow the 
  library to find the rule bundle to be used.  
  
  The rule bundles can be build hierarchically from rules and other rule 
  bundles. This allows libraries to specify rule bundles that can be used by an 
  application that wants to publish their registered objects. If the
  application that links with the library wants to use those rules, it 
  can just add the library's rule bundle to its own rule bundle. 
  
  The rule bundles linked by a rule bundle are added to the configuration 
  before the individual rules in that rule bundle. So, If an application wants 
  to overwrite some rules from the library's bundle, it can just define another 
  rule with the same name in its own bundle.

@subsection MonSvcConfigurationString Textual Specification

  The configuration rules can also be created from a textual representation.
  In the current version, a custom format is used 
  (see @ref monsvc::ConfigurationRule::from(const std::string&)), but upon feedback it 
  can be replaced by another one / XML / JSON / etc.

@section MonSvcExample Usage Example

  An application or library that wants to monitor an object should first 
  register it:

  @code 
  monsvc::MonitoringService::instance().register_object("name", obj);
  @endcode

  If a library only registers objects and leaves the publishing up to the 
  application, it should link against the lightweight version of the library: 
  libmonsvc.so.

  An application that wants to configure the publishing must do so as 
  presented below:

  @code
  monsvc::PublishingController pc;
  shared_ptr<monsvc::ConfigurationRule> rule = monsvc::ConfigurationRule::from(...);
  pc.add_configuration_rule(*rule);

  Configuration conf(...);
  pc.add_configuration_rules(conf);

  pc.start_publishing();
  // ... update the registered objects
  pc.stop_publishing();

  // At the end of the run, dump everything in a ROOT file.
  monsvc::FilePublisher file_publisher(root_file);
  pc.publish_now(file_publisher);
  @endcode

  The application should link against the version of the library that contains
  also the publishing implementation - libmonsvcpub.so.

  Note that although in the example, the monsvc::MonitoringService::register_object, 
  monsvc::PublishingController::add_configuration_rule, 
  monsvc::PublishingController::start_publishing and 
  monsvc::PublishingController::stop_publishing methods are called in this order, 
  there is no restriction on this.
*/


#include "monsvc/detail/Registry.h"

using std::mutex;
using std::lock_guard;

namespace monsvc {

    namespace detail {
        Registry::Registry()
        {}

        bool Registry::remove_object(const std::string& name, const Tag& tag)
        {
            lock_guard<mutex> lock(m_mutex);
            const auto& range = m_objects.equal_range(name);
            const auto& found = 
                std::find_if(range.first, range.second, 
                             [tag](const std::pair<std::string, Entry>& e)
                             { return e.second->tag_info() == tag; });

            if (found != range.second) {
                m_objects.erase(found);
                return true;
            }
            return false;
        }

        int Registry::remove_objects(const std::string& name)
        {
            lock_guard<mutex> lock(m_mutex);
            return m_objects.erase(name);
        }

        void Registry::clear()
        {
            // descriptors are deleted, referenced objects are not !
            lock_guard<mutex> lock(m_mutex);
            m_objects.clear();
        }

        Registry& Registry::instance() {
            static Registry s_instance;
            return  s_instance;
        }

        void Registry::for_each(std::function<void(const Entry)> f) const
        {
            lock_guard<mutex> lock(m_mutex);
            Storage::const_iterator it;
            for (it = m_objects.begin(); it != m_objects.end(); ++it) {
                f(it->second);
            }
        }

        void Registry::publish_object(const std::string& name, 
                                      Entry rep)
        {
            lock_guard<mutex> lock(m_mutex);
            const auto& range = m_objects.equal_range(name);
            const Tag& requested_tag = rep->tag_info();
            
            bool found = 
                std::any_of(range.first, range.second, 
                            [requested_tag]
                            (const std::pair<std::string, Entry>& e)
                            { return e.second->tag_info() == requested_tag; });
            
            if (found) {
                std::string t = requested_tag ? 
                    std::to_string(requested_tag.value):std::string("None");
                throw NameExists(ERS_HERE, name, t);
            }

            m_objects.insert(std::make_pair(name, rep));
        }

        Registry::Entry 
        Registry::find_object(const std::string& name, const Tag& tag) const
        {
            lock_guard<mutex> lock(m_mutex);
            const auto& range = m_objects.equal_range(name);
            const auto& found = 
                std::find_if(range.first, range.second, 
                             [tag](const std::pair<std::string, Entry>& e)
                             { return e.second->tag_info() == tag; });

            if (found == range.second) {
                std::string t = tag ? 
                    std::to_string(tag.value):std::string("None");
                throw NotFound(ERS_HERE, name, t);
            }
            return found->second;
        }

        std::vector<Registry::Entry>
        Registry::find_objects(const std::string& name) const
        {
            lock_guard<mutex> lock(m_mutex);
            std::vector<Registry::Entry> result;
            
            const auto& range = m_objects.equal_range(name);
            for(auto it = range.first; it != range.second; ++it) {
                result.emplace_back(it->second);
            }
            return result;
        }

        bool Registry::remove_if(std::function<bool(Entry)> f)
        {
            lock_guard<mutex> lock(m_mutex);
            for(Storage::iterator it = m_objects.begin(); it != m_objects.end(); ++it) {
                if (f(it->second)) {
                    m_objects.erase(it);
                    return true;
                }
            }
            return false;
        }

    }

}

/* Local Variables: */
/* c-basic-offset:4 */
/* End: */

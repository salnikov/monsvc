#include "monsvc/detail/FilePublisher.h"
#include "monsvc/NameFilter.h"
#include "monsvc/Tag.h"

#include <TObject.h>
#include <TGraph.h>
#include <TH1.h>
#include <is/info.h>

#include <TFile.h>

using std::string;

namespace monsvc {

    FilePublisher::FilePublisher(TFile *root_file)
        : m_root_file(root_file)
    {
    }

    FilePublisher::~FilePublisher()
    {
    }

    void FilePublisher::publish(const string& name, const Tag&,
                                const Annotations&, TObject *obj)
    {
        m_root_file->WriteObject(obj, name.c_str());
    }

    void FilePublisher::publish(const string& name, const Tag&,
                                const Annotations&, TH1 *obj)
    {
        m_root_file->WriteObject(obj, name.c_str());
    }

    void FilePublisher::publish(const string& name, const Tag&,
                                const Annotations&, TGraph *obj)
    {
        m_root_file->WriteObject(obj, name.c_str());
    }
}

/* Local Variables: */
/* c-basic-offset:4 */
/* End: */

/*
 * PublishingScheduler.cpp
 *
 *  Created on: Oct 25, 2012
 *      Author: ctalau
 */

#include "monsvc/detail/PeriodicScheduler.h"
#include "monsvc/detail/Registry.h"
#include "monsvc/Exceptions.h"

#include <boost/noncopyable.hpp>
#include <boost/date_time/gregorian/gregorian_types.hpp>
#include <mutex>
#include <vector>
#include <memory>
#include <functional>
#include <algorithm>
#include <random>

#include <iostream>

using std::lock_guard;
using std::mutex;
using std::make_shared;
using std::shared_ptr;
using boost::thread;
using boost::thread_interrupted;
using std::list;
using std::vector;
using std::function;
using std::hash;
using std::shuffle;
using std::minstd_rand;

using namespace boost::posix_time;
using namespace boost::gregorian;

namespace monsvc {
    namespace detail {

        namespace {
            void collect(const detail::Registry::Entry& obj,
                         vector<detail::Registry::Entry>& objs,
                         const NameFilter& flt,
                         const std::vector<NameFilter>& preceedingFilters)
            {
                for (auto&& filter: preceedingFilters) {
                    if (filter.matches(obj->name())) {
                        // one of preceeding rule filters matches it, we skip it
                        return;
                    }
                }
                if (flt.matches(obj->name())) {
                    objs.push_back(obj);
                }
            }
      
            void randomise(vector<detail::Registry::Entry>& objs)
            {
                minstd_rand gen(::getpid());
                shuffle(objs.begin(), objs.end(), gen);
            }

            void filter_slot(vector<detail::Registry::Entry>& objs,
                             unsigned int slotidx, unsigned int nslots)
            {
                hash<std::string> hash_fn;
        
                auto it = objs.begin();
        
                while (it != objs.end()) {
                    auto hash = hash_fn((*it)->name());
                    if ( hash % nslots != slotidx) {
                        it = objs.erase(it);
                    } else {
                        ++it;
                    }
                }
            }
        }
    
        void PeriodicScheduler::PeriodicTask::publish(unsigned int slotidx, unsigned int nslots)
        {
            vector<detail::Registry::Entry> entries;
            detail::Registry::instance().for_each([this, &entries](Registry::Entry entry) {
                collect(entry, entries, this->m_filter, this->m_preceedingFilters);
            });
      
            // Filter entries based on slot
            filter_slot(entries, slotidx, nslots);
              
            // Randomise the publication order
            randomise(entries);
      
            try {
                vector<detail::Registry::Entry>::iterator it;
                for (it = entries.begin(); it != entries.end(); ++it) {
                    (*it)->publish(*m_publisher);
                }
            } catch (PublishingIssue& issue) {
                // We don't want to kill the thread for a publishing error.
                ers::error(issue);
            }
        }

        void PeriodicScheduler::PeriodicTask::finalpublication()
        {
            vector<detail::Registry::Entry> entries;
            detail::Registry::instance().for_each([this, &entries](Registry::Entry entry) {
                collect(entry, entries, this->m_filter, this->m_preceedingFilters);
            });
            
            try {
                vector<detail::Registry::Entry>::iterator it;
                for (it = entries.begin(); it != entries.end(); ++it) {
                    (*it)->add_annotation(FINALPUBLICATION);
                    (*it)->publish(*m_publisher);
                    (*it)->remove_annotation(FINALPUBLICATION);
                }
            } catch (PublishingIssue& issue) {
                // We don't want to kill the thread for a publishing error.
                ers::error(issue);
            }
        }

        /*
         * Worker thread class that executes tasks periodically.
         *
         * Unlike boost threads, the thread is initially stopped.
         */
        class PeriodicScheduler::WorkerThread : boost::noncopyable {
        public:
            WorkerThread(unsigned int interval, unsigned nslots,
                         PeriodicScheduler::TimeManager& sleeper, bool sync) :
                m_interval(seconds(interval)),
                m_slot_intervals(),
                m_slotidx(0),
                m_nslots(nslots),
                m_time_manager(sleeper),
                m_sync(sync)
            {
                int total_interval_ms = m_interval.total_milliseconds();
                int slot_ms = total_interval_ms/m_nslots;
                m_slot_intervals = vector<time_duration>(m_nslots-1, milliseconds(slot_ms));
                m_slot_intervals.push_back(milliseconds(total_interval_ms - slot_ms*(m_nslots-1)));
            }

            list<PeriodicTaskPtr>::iterator add_task(PeriodicTaskPtr task)
            {
                lock_guard<mutex> lock(m_tasks_mutex);
                return m_tasks.insert(m_tasks.end(), task);
            }

            void del_task(list<PeriodicTaskPtr>::iterator it)
            {
                lock_guard<mutex> lock(m_tasks_mutex);
                m_tasks.erase(it);
            }

            bool is_idle()
            {
                lock_guard<mutex> lock(m_tasks_mutex);
                return m_tasks.empty();
            }

            void start()
            {
              m_thread = make_shared<thread>(&WorkerThread::run_periodically, this);
            }

            void stop()
            {
                m_thread->interrupt();
                m_thread->join();
                m_thread.reset();
            }

        private:
            shared_ptr<thread> m_thread;
            time_duration m_interval;
            std::vector<time_duration> m_slot_intervals;
            unsigned int m_slotidx;
            unsigned int m_nslots;
            PeriodicScheduler::TimeManager& m_time_manager;
            bool m_sync;

            list<PeriodicTaskPtr> m_tasks;
            mutex m_tasks_mutex;
                  
            void sync2epoch();
            void run_periodically();
        };

        void PeriodicScheduler::WorkerThread::sync2epoch()
        {
            //Synchronize to a multiple of the interval
            ptime now = m_time_manager.now();
            long int interval_ms = m_interval.total_milliseconds();
            ptime epoch(date(2000,1,1)); 
            long int now_ms = (now-epoch).total_milliseconds();
            //If now_ms % interval_ms == 0 we skip a cycle. Too bad.
            time_duration sync(milliseconds(interval_ms - (now_ms % interval_ms)));
            //Do not sync if we are below 50 ms
            if (sync.total_milliseconds() > 50){
              m_time_manager.sleep(sync);
            }
        }
      
        void PeriodicScheduler::WorkerThread::run_periodically()
        {
            if (m_sync) sync2epoch();
         
            ptime next_sleep = m_time_manager.now();
                        
            while (true) {
                list<PeriodicTaskPtr> tasks;
                {
                    lock_guard<mutex> lock(m_tasks_mutex);
                    tasks = m_tasks;
                }

                try {
                    for (list<PeriodicTaskPtr>::iterator it = tasks.begin(); it != tasks.end(); ++it) {
                        // Check for interruptions before trying to execute the task.
                        boost::this_thread::interruption_point();
                        try {
                            (*it)->publish(m_slotidx, m_nslots);
                        } catch(...) {
                            ers::warning(UnexpectedPublishingIssue(ERS_HERE));
                        }
                    }

                    // Sleep until the next publishing deadline skipping the missed deadlines.
                    ptime now = m_time_manager.now();
                    next_sleep += m_slot_intervals[m_slotidx];

                    time_duration excess_time = now - next_sleep;
                    if (!excess_time.is_negative()) {
                        ers::warning(
                                PublishingDeadlineMissedIssue(ERS_HERE, m_interval, excess_time));
                        while (next_sleep < now) {
                            // Make sure we skip the corresponding slot
                            m_slotidx++;
                            m_slotidx = m_slotidx % m_nslots;
                            
                            next_sleep += m_slot_intervals[m_slotidx];
                           
                            // Update the current time
                            now = m_time_manager.now();
                        }
                    }

                    m_time_manager.sleep(next_sleep - now);
                    
                     // Go to the next slot, wrapping up as needed
                    m_slotidx++;
                    m_slotidx = m_slotidx % m_nslots;
                } catch (thread_interrupted& ex) {
                    return;
                }
            }
        }

        // The time manager that uses functions from boost.
        class ThreadTimeManager : public PeriodicScheduler::TimeManager {
        public:
            static ThreadTimeManager& instance() {
                static ThreadTimeManager time_manager;
                return time_manager;
            }
        private:
            void sleep(time_duration duration)
            {
                boost::this_thread::sleep(duration);
            }

            ptime now() {
                return microsec_clock::local_time();
            }
        };


        PeriodicScheduler::PeriodicScheduler() :
                m_thread_map(), m_running(false), m_time_manager(ThreadTimeManager::instance())
        {
        }

        PeriodicScheduler::PeriodicScheduler(TimeManager& time_manager) :
                m_thread_map(), m_running(false), m_time_manager(time_manager)
        {
        }

        PeriodicScheduler::~PeriodicScheduler()
        {
            if (m_running) {
                stop();
            }
        }


        void PeriodicScheduler::start()
        {
            if (!m_running) {
                for (ThreadMap::iterator it = m_thread_map.begin(); it != m_thread_map.end(); ++it) {
                    shared_ptr<WorkerThread> worker = it->second;
                    worker->start();
                }
                m_running = true;
            } else {
                InvalidCommandRunning issue(ERS_HERE);
                ers::warning(issue);
            }
        }

        void PeriodicScheduler::stop()
        {
            if (m_running) {
                for (ThreadMap::iterator it = m_thread_map.begin(); it != m_thread_map.end(); ++it) {
                    shared_ptr<WorkerThread> worker = it->second;
                    worker->stop();
                }
                m_running = false;
            } else {
                InvalidCommandStopped issue(ERS_HERE);
                ers::warning(issue);
            }
        }

      PeriodicScheduler::PeriodicTaskHandle PeriodicScheduler::schedule_task(PeriodicTaskPtr task, unsigned interval, unsigned nslots, bool sync)
        {
            if (nslots == 0) {
                throw std::invalid_argument("number of slots");
            }

            if (interval == 0) {
                throw std::invalid_argument("interval");
            }

            // Create a new thread if there wasn't one already for our publish interval.
            bool newly_created = false;
            WorkerType key(interval, nslots);
                
            ThreadMap::iterator it = m_thread_map.find(key);
            
            if (it == m_thread_map.end()) {
                shared_ptr<WorkerThread> worker(
                                                make_shared<WorkerThread>(interval, nslots, std::ref(m_time_manager), sync));
                it = m_thread_map.insert(std::make_pair(key, worker)).first;
                newly_created = true;
            }

            // Add the task to the thread's queue and start the thread if necessary.
            shared_ptr<WorkerThread> worker = it->second;
            list<PeriodicTaskPtr>::iterator task_it = worker->add_task(task);
            if (m_running && newly_created) {
                worker->start();
            }

            return std::make_pair(key, task_it);
        }

        void PeriodicScheduler::deschedule_task(PeriodicScheduler::PeriodicTaskHandle& task_handler)
        {
            ThreadMap::iterator it = m_thread_map.find(task_handler.first);
            if (it != m_thread_map.end()) {
                shared_ptr<WorkerThread> worker = it->second;
                worker->del_task(task_handler.second);
                if (worker->is_idle()) {
                    if (m_running) {
                        worker->stop();
                    }
                    m_thread_map.erase(it);
                }
            }
        }
    }
} /* namespace monsvc */

/* Local Variables: */
/* c-basic-offset:4 */
/* End: */

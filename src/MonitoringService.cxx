/*
 * MonitoringService.cxx
 *
 *  Created on: Oct 11, 2012
 *      Author: ctalau
 */

#include "monsvc/MonitoringService.h"

#include "monsvc/detail/Registry.h"

using std::string;

namespace monsvc {
    
    MonitoringService& MonitoringService::instance() {
        static MonitoringService instance;
        return instance;
    }
    
    MonitoringService::MonitoringService() : 
        m_registry(detail::Registry::instance()) {}


    bool MonitoringService::remove_object(const string& name) 
    {
        Tag t;
        return m_registry.remove_object(name, t);
    }

    bool MonitoringService::remove_object(const char* name)
    {
        return this->remove_object(std::string(name));
    }

    bool MonitoringService::remove_object(const string& name, int tag) 
    {
        Tag t(tag);
        return m_registry.remove_object(name, t);
    }

    bool MonitoringService::remove_object(const char* name, int tag)
    {
        return this->remove_object(std::string(name), tag);
    }

    int MonitoringService::remove_objects(const string& name) 
    {
        return m_registry.remove_objects(name);
    }

    int MonitoringService::remove_objects(const char* name)
    {
        return this->remove_objects(std::string(name));
    }

    bool MonitoringService::remove_if(std::function<bool(DescriptorPtr)> f)
    {
        return m_registry.remove_if(f);
    }

    MonitoringService::DescriptorPtr 
    MonitoringService::find_object(const std::string& name, const Tag& tag) const
    {
        return m_registry.find_object(name, tag);
    }

    void MonitoringService::publish_object(const std::string& name, 
                                           DescriptorPtr rep)
    {
        m_registry.publish_object(name, rep);
    }

} /* namespace monsvc */

/* Local Variables: */
/* c-basic-offset:4 */
/* End: */

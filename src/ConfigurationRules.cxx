/*
 * ConfigurationRules.cpp
 *
 *  Created on: Oct 26, 2012
 *      Author: ctalau
 */

#include "monsvc/ConfigurationRules.h"
#include "monsvc/detail/ISPublisher.h"
#include "monsvc/detail/OHPublisher.h"
#include "monsvc/detail/OHCommander.h"

#include "monsvcdal/ConfigurationRule.h"
#include "monsvcdal/PublishingParameters.h"
#include "monsvcdal/OnlinePublishingParameters.h"
#include "monsvcdal/ISPublishingParameters.h"
#include "monsvcdal/OHPublishingParameters.h"

#include <config/Configuration.h>
#include <ipc/partition.h>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/algorithm/string/regex.hpp>
#include <boost/regex.hpp>
#include <sstream>
#include <memory>

using boost::regex;

using std::string;
using std::stringstream;
using std::shared_ptr;
using std::make_shared;

using boost::property_tree::ptree;

namespace monsvc {

    // Configuration rule for IS publishing.
    class ISConfigurationRule : public ConfigurationRule {
    public:
        ISConfigurationRule(const string& name, const NameFilter& filter,
                            unsigned interval, unsigned nslots,
                            const string& server_name)
            : ConfigurationRule(name, filter, interval, nslots, server_name),
              server_name(server_name) {}

        virtual shared_ptr<PublisherBase> create_publisher(const IPCPartition& partition,
                const string& app_name) const {
            return make_shared<ISPublisher>(partition, server_name, app_name);
        }

        const string& get_server_name() const { return server_name; }
    private:
        virtual const string params_str() const;

        const string server_name;
    };

    const string ISConfigurationRule::params_str() const
    {
        stringstream repr;
        repr << "is" << ":(" << get_interval() 
             << ',' << get_nslots() 
             << ',' << server_name 
             << ')';
        return repr.str();
    }

    // Configuration rule for OH publishing.
    class OHConfigurationRule : public ConfigurationRule {
    public:
        OHConfigurationRule(const string& name, const NameFilter& filter,
                            unsigned interval, unsigned nslots,
                            const string& server_name,
                            const string& provider_name)
            : ConfigurationRule(name, filter, interval, nslots, server_name),
              server_name(server_name),
              provider_name(provider_name) {}

        virtual shared_ptr<PublisherBase> create_publisher(const IPCPartition& partition,
                const string&) const {
            auto cmdlst = 
                monsvc::detail::create_commander(server_name, provider_name,
                                                 this->get_filter());
            return make_shared<OHPublisher>(partition, 
                                            server_name, 
                                            provider_name,
                                            cmdlst);
        }

        const string& get_server_name() const { return server_name; }

        const string& get_provider_name() const { return provider_name; }
    private:
        virtual const string params_str() const;

        const string server_name;
        const string provider_name;
    };

    const string OHConfigurationRule::params_str() const
    {
        stringstream repr;
        repr << "oh" << ":(" << get_interval() 
             << ',' << get_nslots()
             << ',' << get_server_name() 
             << ',' << get_provider_name()
             << ')';
            return repr.str();
    }

    namespace{
        std::string parse_server(const std::string & server){
            
            //${<VARIABLE_NAME>[=<DEFAULT_SERVER_NAME>]}[<SUFFIX>]

            auto server_name = server;
            std::string suffix("");
            static const regex server_format("\\$\\{(.+)\\}(.*)");
            
            boost::smatch match_result;
            if (boost::regex_match(server, match_result, server_format)) {
                auto server_info = match_result[1].str();
                auto pos = server_info.find('=');
                auto variable = server_info.substr(0,pos);
                auto default_server = pos==string::npos?
                    "":server_info.substr(pos+1,string::npos);
                
                auto serverenv = std::getenv(variable.c_str());
                server_name = serverenv? serverenv:default_server;
                
                if (server_name == ""){
                    throw BadRuleFormat(ERS_HERE, "Unknown", 
                                        "wrong server field --> "+server);
                }
                
                if (match_result.size() == 3) {
                    suffix = match_result[2].str();
                }
            } 
            
            return server_name+suffix;
        }
    }

    shared_ptr<ConfigurationRule> ConfigurationRule::from(const string& repr)
    {
        static const regex is_rule_format("(.*):(.*)/(.*)=>is:\\((.*),(.*),(.*)\\)");
        static const regex oh_rule_format("(.*):(.*)/(.*)=>oh:\\((.*),(.*),(.*),(.*)\\)");

        shared_ptr<ConfigurationRule> result;
        boost::smatch match_result;

        if (boost::regex_match(repr, match_result, is_rule_format)) {
            
            result.reset(new ISConfigurationRule(
                    match_result[1].str(),
                    NameFilter(match_result[2].str(), match_result[3].str()),
                    boost::lexical_cast<int>(match_result[4].str()),
                    boost::lexical_cast<int>(match_result[5].str()),
                    parse_server(match_result[6].str())
               ));
        } else if (boost::regex_match(repr, match_result, oh_rule_format)) {
            result.reset(new OHConfigurationRule(
                    match_result[1].str(),
                    NameFilter(match_result[2].str(), match_result[3].str()),
                    boost::lexical_cast<int>(match_result[4].str()),
                    boost::lexical_cast<int>(match_result[5].str()),
                    parse_server(match_result[6].str()),
                    parse_server(match_result[7].str())
            ));
        } else {
            BadRuleFormat issue(ERS_HERE, repr, "cannot parse");
            throw issue;
        }


        return result;
    }

    shared_ptr<ConfigurationRule> ConfigurationRule::from(const ptree & rule)
    {
        shared_ptr<ConfigurationRule> result;

        try {
            auto incFilter = rule.get<std::string>("IncludeFilter");
            auto excFilter = rule.get<std::string>("ExcludeFilter");
            NameFilter filter(incFilter, excFilter);
            
            auto name = rule.get<std::string>("Name");
            auto UID = rule.get<std::string>("UID");
            
            name = name.empty() ? UID : name;
            
            boost::optional<const ptree&> ohconfig = 
                rule.get_child_optional("Parameters.OHPublishingParameters");
            boost::optional<const ptree&> isconfig = 
                rule.get_child_optional("Parameters.ISPublishingParameters");

            if (isconfig) {
                auto interval = (*isconfig).get<unsigned int>("PublishInterval");
                auto nslots = (*isconfig).get<unsigned int>("NumberOfSlots");
                auto isserver = (*isconfig).get<std::string>("ISServer");
            
                result.reset(new ISConfigurationRule(
                                                     name, filter,
                                                     interval, nslots,
                                                     parse_server(isserver)));
            } else if (ohconfig) {
                auto interval = (*ohconfig).get<unsigned int>("PublishInterval");
                auto nslots = (*ohconfig).get<unsigned int>("NumberOfSlots");
                auto ohserver = (*ohconfig).get<std::string>("OHServer");
                auto rootprovider = (*ohconfig).get<std::string>("ROOTProvider");
            
                result.reset(new OHConfigurationRule(
                                                     name, filter,
                                                     interval, nslots,
                                                     parse_server(ohserver),
                                                     parse_server(rootprovider)));
            } else {
                BadRuleType issue(ERS_HERE, UID, "Unknown");
                throw issue;
            }
        } catch (boost::property_tree::ptree_bad_path& ex) {
            std::stringstream ss;
            boost::property_tree::json_parser::write_json(ss, rule);
            BadRuleFormat issue(ERS_HERE, "ptree-based", ss.str(), ex);
            throw issue;
        } catch (boost::property_tree::ptree_bad_data& ex) {
            std::stringstream ss;
            boost::property_tree::json_parser::write_json(ss, rule);
            BadRuleFormat issue(ERS_HERE, "ptree-based", ss.str(), ex);
            throw issue;
        }

        return result;
    }

    shared_ptr<ConfigurationRule> ConfigurationRule::from(Configuration& conf,
            const dal::ConfigurationRule * rule)
    {
        shared_ptr<ConfigurationRule> result;
        const dal::OnlinePublishingParameters * params =
                conf.cast<dal::OnlinePublishingParameters>(rule->get_Parameters());

        if (!params) {
            BadRuleType issue(ERS_HERE, rule->UID(), rule->s_class_name);
            throw issue;
        }

        unsigned interval(params->get_PublishInterval());
        unsigned nslots(params->get_NumberOfSlots());
        NameFilter filter(rule->get_IncludeFilter(), rule->get_ExcludeFilter());
        string name(rule->get_Name().empty() ? rule->UID() : rule->get_Name());

        if (const dal::ISPublishingParameters* is_params =
                conf.cast<dal::ISPublishingParameters>(params)) {
            result.reset(new ISConfigurationRule(
                    name, filter, interval, nslots,
                    parse_server(is_params->get_ISServer())
            ));
        } else if (const dal::OHPublishingParameters * oh_params =
                conf.cast<dal::OHPublishingParameters>(params)) {
            result.reset(new OHConfigurationRule(
                    name, filter, interval, nslots,
                    parse_server(oh_params->get_OHServer()),
                    parse_server(oh_params->get_ROOTProvider())
                    ));
        }

        return result;
    }

    const string ConfigurationRule::str() const
    {
        stringstream repr;
        repr << get_name() << ':' << get_filter().str() << "=>" << params_str();
        return repr.str();
    }

} /* namespace monsvc */

/* Local Variables: */
/* c-basic-offset:4 */
/* End: */


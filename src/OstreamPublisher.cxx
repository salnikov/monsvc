
#include "monsvc/detail/OstreamPublisher.h"
#include "monsvc/Exceptions.h"
#include "monsvc/Tag.h"

#include <TObject.h>
#include <is/info.h>
#include <iostream>

using std::ostream;
using std::string;
using std::cout;
using std::endl;

namespace monsvc {

    OstreamPublisher::OstreamPublisher(ostream& os)
        : m_stream(os)
    {
        if (&os != &cout) {
            ers::warning(CustomOstreamPublishingIssue(ERS_HERE));
        }
    }

    OstreamPublisher::~OstreamPublisher()
    {
    }

    void 
    OstreamPublisher::publish(const string& name, const Tag& tag,
                              const Annotations& ann, TObject *obj)
    {
        std::string t = tag ? std::to_string(tag.value):std::string("None");
        m_stream << "Name: " << name << " Tag: " << t << endl;
        m_stream << "Annotations:" << std::endl;
        for (const auto& a: ann) {
            std::cout << a.first << ":" << a.second << std::endl;
        }
        obj->Print();
    }

    void 
    OstreamPublisher::publish(const string& name, const Tag& tag,
                              const Annotations& ann, ISInfo *obj)
    {
        std::string t = tag ? std::to_string(tag.value):std::string("None");
        m_stream << "Name: " << name << " Tag: " << t << endl;
        m_stream << "Annotations:" << std::endl;
        for (const auto& a: ann) {
            std::cout << a.first << ":" << a.second << std::endl;
        }
        obj->print(m_stream);
    }
}

/* Local Variables: */
/* c-basic-offset:4 */
/* End: */


#include "monsvc/detail/TInfo.h"

#include <TClass.h>
#include <TDataMember.h>
#include <TDataType.h>
#include <TList.h>

namespace monsvc {

    namespace detail {

        template<class T>
        T value(TObject *obj, Long_t offset)
        {
            return *reinterpret_cast<T*>(reinterpret_cast<unsigned char*>(obj) + offset);
        }

        TInfo::TInfo(TObject *obj)
            : ISInfo(obj->ClassName()),
              m_obj(obj)
        {}


        void TInfo::publishGuts(ISostream& s)
        {
            TClass *cl     = m_obj->IsA();

            TIter next(cl->GetListOfDataMembers());
            while(TObject *p = next()) {

                if(TDataMember* memptr = dynamic_cast<TDataMember*>(p)) {
                    Long_t offset = memptr->GetOffset();
                    if(memptr->IsBasic()) {
                        switch(memptr->GetDataType()->GetType()) {
                        case kChar_t:
                            s << value<char>(m_obj, offset);
                            break;
                        case kUChar_t:
                            s << value<unsigned char>(m_obj, offset);
                            break;
                        case kchar:
                            s << value<char>(m_obj, offset);
                            break;

                        case kShort_t:
                            s << value<short>(m_obj, offset);
                            break;
                        case kUShort_t:
                            s << value<unsigned short>(m_obj, offset);
                            break;
                        case kInt_t:
                            s << value<int>(m_obj, offset);
                            break;
                        case kUInt_t:
                            s << value<unsigned int>(m_obj, offset);
                            break;
                        case kLong_t:
                            s << value<long>(m_obj, offset);
                            break;
                        case kULong_t:
                            s << value<unsigned long>(m_obj, offset);
                            break;

                        case kFloat_t:
                        case kFloat16_t:
                            s << value<float>(m_obj, offset);
                            break;

                        case kDouble_t:
                        case kDouble32_t:
                            s << value<double>(m_obj, offset);
                            break;


                        case kBool_t:
                            s << value<bool>(m_obj, offset);
                            break;

                        case kLong64_t:
                            s << value<int64_t>(m_obj, offset);
                            break;

                        case kULong64_t:
                            s << value<uint64_t>(m_obj, offset);
                            break;


                        case kCharStar:
                            s << value<const char *>(m_obj, offset);
                            break;

                        case kOther_t:
                        case kNoType_t:
                        case kCounter:
                        case kBits:
                            break;
                        }
                    }
                }
            }

        }

        void TInfo::refreshGuts(ISistream& )
        {
            // ERS_WARN("TInfo::refreshGuts is not implemented");
        }
    }
}

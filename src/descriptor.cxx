
#include "monsvc/descriptor.h"

namespace monsvc {
  
  const Annotations& descriptor_base::annotations() const { 
    return m_annotations; 
  }

  void descriptor_base::add_annotation(const Annotation& annotation) {  
    m_annotations.push_back(annotation); 
  }

  void descriptor_base::add_annotations(const Annotations& annotations) {  
    m_annotations.insert(m_annotations.begin(), 
                         annotations.begin(), annotations.end()); 
  }
  
  void descriptor_base::remove_annotation(const Annotation& annotation) { 
    m_annotations.erase(std::remove(m_annotations.begin(), m_annotations.end(), annotation), m_annotations.end());
  }

  void descriptor_base::remove_annotations(const Annotations& annotations) { 
    for (const auto& a: annotations) {
      m_annotations.erase(std::remove(m_annotations.begin(), m_annotations.end(), a), m_annotations.end());
    }
  }
  
  void descriptor_base::reset_annotations() {
    m_annotations.clear(); 
  }

}

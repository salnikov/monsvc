/*
 * OnlineMonitoringConfigurator.cpp
 *
 *  Created on: Oct 24, 2012
 *      Author: ctalau
 */

#include "monsvc/PublishingController.h"
#include "monsvc/NameFilter.h"

#include "monsvc/detail/Registry.h"
#include "monsvc/detail/PeriodicScheduler.h"

#include "monsvcdal/ConfigurationRuleBundle.h"
#include "monsvcdal/ConfigurationRule.h"
#include "monsvcdal/PublishingParameters.h"
#include "monsvcdal/ISPublishingParameters.h"
#include "monsvcdal/OHPublishingParameters.h"

#include <boost/property_tree/ptree.hpp>

#include <mutex>
#include <config/Configuration.h>
#include <ipc/partition.h>
#include <map>
#include <utility>
#include <algorithm>
#include <random>
#include <functional>

using std::lock_guard;
using std::recursive_mutex;
using std::make_shared;
using std::shared_ptr;

using std::string;
using std::map;
using std::pair;
using std::make_pair;
using std::vector;

using boost::property_tree::ptree;

namespace monsvc {

namespace detail {

/**
 * Helper struct to facilitate ordering of the rules.
 *
 * This just copies more or less complete content of the ConfigurationRule,
 * purpose is to avoid keeping the copy of the rule which would need a clone
 * method that does not exist now. It also knows how to order rules based on
 * their contents.
 */
struct OrderedConfigurationRule {

    OrderedConfigurationRule(const ConfigurationRule& rule,
                             const shared_ptr<PublisherBase>& publisher_)
        : name(rule.get_name()),
          repr(rule.str()),
          server(rule.get_server()),
          interval(rule.get_interval()),
          nslots(rule.get_nslots()),
          filter(rule.get_filter()),
          publisher(publisher_)
    {
    }

    // defines ordering of the rules
    bool operator<(const OrderedConfigurationRule& other) {
        // potentially we could define ordering/priority explicitly in the
        // configuration but it's not done now and it would need significant
        //  changes everywhere. For now ordering is based on the existing
        // features, namely update frequency.  In TDAQ we define default
        // "slow" rule and one or few fast rules, this means that we want
        // fast rules to have higher priority.
        return interval < other.interval;
    }

    string name;
    string repr;
    string server;
    unsigned interval;
    unsigned nslots;
    NameFilter filter;
    shared_ptr<PublisherBase> publisher;
    detail::PeriodicScheduler::PeriodicTaskPtr task;
};

} // namespace detail

    class PublishingController::Impl {

    /* pImpl class for PublishingController. */
    public:
        Impl(const IPCPartition& partition, const string& app_name) :
            m_partition(partition),
            m_app_name(app_name)
        {
        }

        void add_configuration_rules(Configuration& conf);
        void add_configuration_rules(Configuration& conf,
                                     const dal::PublishingApplication* app);
        void add_configuration_rules(const ptree &bundle);
        void add_configuration_rule(const ptree &rule);
        void add_configuration_rule(const ConfigurationRule& rule);
        void disable_configuration_rule(const string& rule_name);
        const vector<string> get_rules() const;
        void rebuild_tasks();
        void start_publishing();
        void stop_publishing();
        void publish_all();


        const IPCPartition& m_partition;
        const string m_app_name;

        detail::PeriodicScheduler m_scheduler;
        vector<detail::PeriodicScheduler::PeriodicTaskHandle> m_task_handles;
        map<string, detail::OrderedConfigurationRule> m_stored_rules;
        bool m_publishing = false;
        recursive_mutex m_mutex;
    };

    namespace {
        IPCPartition& get_partition()
        {
          static IPCPartition partition(getenv("TDAQ_PARTITION"));
          return partition;
        }
    }

    PublishingController::PublishingController(const IPCPartition& partition, 
                                               const string& app_name)
            : m_impl(new PublishingController::Impl(partition, app_name))
    {
    }

    PublishingController::PublishingController() : m_impl(
            new PublishingController::Impl(get_partition(), 
                                           getenv("TDAQ_APPLICATION_NAME")))
    {
    }

    PublishingController::~PublishingController() {}


    namespace {
        void collect_rules(const dal::ConfigurationRuleBundle * bundle,
                map<const string, const dal::ConfigurationRule *>& rules_buf)
        {
            const vector<const dal::ConfigurationRuleBundle *>& bundles =
                    bundle->get_LinkedBundles();
            for (const auto &it: bundles)
            {
                collect_rules(it, rules_buf);
            }

            const vector<const dal::ConfigurationRule *>& rules = 
                bundle->get_Rules();
            for (const auto &it: rules)
            {
                rules_buf.insert(make_pair((it)->UID(), it));
            }

        }
    }

    void PublishingController::Impl::
    add_configuration_rules(Configuration& conf)
    {
        const dal::PublishingApplication* app = conf.get<dal::PublishingApplication>(m_app_name);
        if(app)
            this->add_configuration_rules(conf, app);
        else
            throw ApplicationNotFound(ERS_HERE, m_app_name);
    }

    void PublishingController::Impl::
    add_configuration_rules(Configuration& conf,
                            const dal::PublishingApplication* app)
    {
        map<const string, const dal::ConfigurationRule *> rules_buf;
        collect_rules(app->get_ConfigurationRules(), rules_buf);

        for (const auto &it: rules_buf) {
            try {
                add_configuration_rule(*ConfigurationRule::from(conf, it.second));
            } catch (const BadRuleType& exception) {
                // Issue an warning and ignore this rule.
                ers::warning(exception);
            } catch (const BadRuleFormat& exception) {
                // Issue an warning and ignore this rule.
                ers::warning(exception);
            }
        }
    }

    void PublishingController::Impl::
    add_configuration_rule(const ConfigurationRule& rule) 
    {
        lock_guard<recursive_mutex> lock(m_mutex);

        // rules can be added only when not running, do not kill it though, just give a warning
        if (m_publishing) {
            ers::warning(InvalidAddRuleRunning(ERS_HERE, rule.str()));
            return;
        }

        // there are unit tests that expect that this check is made in this method
        if (rule.get_interval() == 0 or rule.get_nslots() == 0) {
            InvalidInterval issue(ERS_HERE, rule.get_name());
            ers::warning(issue); // we simply don't use this rule
            return;
        }

        auto publisher = rule.create_publisher(m_partition,  m_app_name);

        // this will drop old rule with the same name
        m_stored_rules.insert_or_assign(rule.get_name(), detail::OrderedConfigurationRule(rule, publisher));

    }

    namespace {
        void collect_rules(const ptree & bundle,
                           vector<boost::property_tree::ptree::const_assoc_iterator> & rules_buf){
            
            try {
                auto bundles = bundle.get_child("LinkedBundles").equal_range("ConfigurationRuleBundle");
                for ( auto it = bundles.first; it != bundles.second; ++it) {
                    collect_rules(it->second, rules_buf);
                }
            } catch (const boost::property_tree::ptree_bad_path& ex) {
                //Some lists may be empty in OKS and therefore not exist here
            } 

            try {
                auto rules = bundle.get_child("Rules").equal_range("ConfigurationRule");
                for ( auto it = rules.first; it != rules.second; ++it) {
                    rules_buf.push_back(it);
                }
            }catch (const boost::property_tree::ptree_bad_path& ex) {
                //Some lists may be empty in OKS and therefore not exist here
            } 
        }
    }

    void PublishingController::Impl::
    add_configuration_rules(const ptree & bundle)
    {
        vector<boost::property_tree::ptree::const_assoc_iterator> rules_buf;
        collect_rules(bundle, rules_buf);

        for (const auto &it: rules_buf) {
            try {
                add_configuration_rule(*ConfigurationRule::from(it->second));
            } catch (const BadRuleType& exception) {
                // Issue an warning and ignore this rule.
                ers::warning(exception);
            } catch (const BadRuleFormat& exception) {
                // Issue an warning and ignore this rule.
                ers::warning(exception);
            }
        }

    }
    
    void PublishingController::Impl::
    add_configuration_rule(const ptree &rule)
    {
        try {
            add_configuration_rule(*ConfigurationRule::from(rule));
        } catch (const BadRuleType& exception) {
            // Issue an warning and ignore this rule.
            ers::warning(exception);
        } catch (const BadRuleFormat& exception) {
            // Issue an warning and ignore this rule.
            ers::warning(exception);
        }
    }

    void PublishingController::Impl::
    disable_configuration_rule(const string& rule_name) 
    {
        lock_guard<recursive_mutex> lock(m_mutex);

        // rules can be removed only when not running, do not kill it though, just give a warning
        if (m_publishing) {
            ers::warning(InvalidAddRuleRunning(ERS_HERE, rule_name));
            return;
        }

        m_stored_rules.erase(rule_name);
    }

    const vector<string> PublishingController::Impl::get_rules() const
    {
        vector<string> result;
        for (auto&& rule_pair: m_stored_rules) {
            result.push_back(rule_pair.second.repr);
        }
        return result;
    }


    void PublishingController::Impl::rebuild_tasks()
    {
        // Create a task for each OrderedConfigurationRule, the tasks need to
        // know about NameFilters of the preceeding tasks (from higher priority
        // rules) publishing to the same server, so split all rules by the
        // server name first and then order the rules.

        map<string, vector<detail::OrderedConfigurationRule*>> server2rules;
        for (auto&& rule_pair: m_stored_rules) {
            auto&& rule = rule_pair.second;
            server2rules[rule.server].push_back(&rule);
        }

        for (auto&& pair: server2rules) {
            // empty server name is handled below
            if (pair.first.empty()) {
                continue;
            }
            auto&& rules = pair.second;
            std::stable_sort(rules.begin(), rules.end(),
                [](detail::OrderedConfigurationRule* lhs, detail::OrderedConfigurationRule* rhs) {
                    return *lhs < *rhs;
                });

            // collect filters from subsequent rules
            vector<NameFilter> filters;
            for (auto&& rule: rules) {
                auto task = make_shared<detail::PeriodicScheduler::PeriodicTask>(rule->publisher, rule->filter, filters);
                rule->task = task;
                filters.push_back(rule->filter);
            }
        }

        // empty server means it's not OH/IS publisher, we do not care
        // about exclusivity.
        for (auto&& rule: server2rules[""]) {
            auto task = make_shared<detail::PeriodicScheduler::PeriodicTask>(rule->publisher, rule->filter);
            rule->task = task;
        }
    }

    void PublishingController::Impl::start_publishing()
    {
        lock_guard<recursive_mutex> lock(m_mutex);

        // make tasks for all rules
        rebuild_tasks();

        // schedule all tasks
        for (auto&& rule_pair: m_stored_rules) {
            auto&& rule = rule_pair.second;
            try {
                auto handle = m_scheduler.schedule_task(rule.task, rule.interval, rule.nslots, true);
                m_task_handles.push_back(handle);
            } catch (const std::invalid_argument& exception) {
                InvalidInterval issue(ERS_HERE, rule.name);
                ers::warning(issue); // we simply don't use this rule
            }
        }

        m_scheduler.start();
        m_publishing = true;
    }

    void PublishingController::Impl::stop_publishing()
    {
        lock_guard<recursive_mutex> lock(m_mutex);

        m_scheduler.stop();

        // deschedule all tasks, and clear their handles (they are invalidated)
        for (auto&& handle: m_task_handles) {
            m_scheduler.deschedule_task(handle);
        }
        m_task_handles.clear();

        m_publishing = false;
    }

    void PublishingController::Impl::publish_all()
    {
        lock_guard<recursive_mutex> lock(m_mutex);

        // if publish_all() is called before start_publishing() then tasks
        // are not populated yet
        if (!m_stored_rules.empty()) {
            auto it = m_stored_rules.begin();
            if (it->second.task == nullptr) {
                rebuild_tasks();
            }
        }

        for (auto&& rule_pair: m_stored_rules){
            if (rule_pair.second.task != nullptr) {
                rule_pair.second.task->finalpublication();
            }
        }
    }


    // Methods forwarding to the implementation.
    void PublishingController::start_publishing()
    {
        m_impl->start_publishing();
    }

    void PublishingController::stop_publishing()
    {
        m_impl->stop_publishing();
    }
    
    void PublishingController::publish_all()
    {
        m_impl->publish_all();
    }

    const vector<string> PublishingController::get_rules() const
    {
        return m_impl->get_rules();
    }

    void PublishingController::
    add_configuration_rule(const ConfigurationRule& rule)
    {
        return m_impl->add_configuration_rule(rule);
    }

    void PublishingController::add_configuration_rules(Configuration& conf)
    {
        m_impl->add_configuration_rules(conf);
    }

    void PublishingController::
    add_configuration_rules(Configuration& conf,
                            const dal::PublishingApplication* app)
    {
        m_impl->add_configuration_rules(conf, app);
    }

    void PublishingController::add_configuration_rules(const ptree &bundle)
    {
        m_impl->add_configuration_rules(bundle);
    }
        
    void PublishingController::add_configuration_rule(const ptree &rule)
    {
        m_impl->add_configuration_rule(rule);
    }

    void PublishingController::
    disable_configuration_rule(const string& rule_name)
    {
        return m_impl->disable_configuration_rule(rule_name);
    }
}

/* Local Variables: */
/* c-basic-offset:4 */
/* End: */

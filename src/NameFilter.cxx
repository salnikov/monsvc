/*
 * PublishingFilter.cpp
 *
 *  Created on: Oct 22, 2012
 *      Author: ctalau
 */

#include "monsvc/NameFilter.h"
#include "monsvc/Exceptions.h"

#include <sstream>

using std::string;
using std::stringstream;
using boost::regex_match;

namespace monsvc {

    NameFilter::NameFilter(const string& include_filter, 
                           const string& exclude_filter){ 
      
      try{
        m_include_filter = boost::regex(include_filter);
      }catch(boost::bad_expression &e){
        std::string action("Include filter set to '.*'");
        MalformedRegEx issue(ERS_HERE, include_filter, action, e);
        ers::error(issue);
        m_include_filter = boost::regex(".*");
      }
      try{
        m_exclude_filter = boost::regex(exclude_filter);
      }catch(boost::bad_expression &e){
        std::string action("Exclude filter set to none");
        MalformedRegEx issue(ERS_HERE, exclude_filter, action, e);
        ers::error(issue);
        m_exclude_filter = boost::regex("");
      }
    }

    bool NameFilter::matches(const string& name) const
    {
        return regex_match(name, m_include_filter)
            && !regex_match(name, m_exclude_filter);
    }

    const string NameFilter::str() const {
        stringstream repr;
        repr << m_include_filter.str() << '/' << m_exclude_filter.str();
        return repr.str();
    }

} /* namespace monsvc */

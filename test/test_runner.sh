#!/bin/bash

#
# Sets up the environment for a test application to be run.
# It starts the required servers and exports the environment
# variables.
# 
# It expects to be run from the $(bin) directory.
#

BIN_PATH=$(pwd)
SLEEP_TIME=10000

# Start a server and waits for it to actually start serving.
function start_server {
    START_SERVER_COMMAND=$1
    if [ $# -eq 1 ]
    then 
        TEST_SERVER_COMMAND="test_$1"
    else
        TEST_SERVER_COMMAND=$2
    fi

    $START_SERVER_COMMAND &

    for try in $(seq 1 10)
    do
        python -c "import time; time.sleep(0.1)"
        $TEST_SERVER_COMMAND
        if [ $? -eq 0 ]
        then 
            return
        fi
    done
    echo "ERROR: Could not start server $1"
}


# Tear down all servers.
function cleanup {
    echo "*** Tearing down the partition ..."
    is_rm -v -p $TDAQ_PARTITION -n "DF" -r ".*" || true
    ipc_rm -f -i ".*" -n ".*" -p $TDAQ_PARTITION || true
    ipc_rm -f -i ".*" -n ".*" -p initial || true
    rm -f $IPC_INIT_REF_FILE || true
    pkill -9 -P $$
    echo "*** done."
}

SETUP_PERFORMED=0

# Sets up the environment
function setup_env {
    echo -n "*** Setting up the partition environment ... "
    export TDAQ_PARTITION=monsvc_test_part
    export TDAQ_APPLICATION_NAME=monsvc_test_app
    export TDAQ_IS_SERVER_NAME=DF
    export LD_LIBRARY_PATH=$BIN_PATH:$LD_LIBRARY_PATH

# Use a local ref file for the initial partition.
    IPC_INIT_REF_FILE="$BIN_PATH/ipc_init.ref"
    touch $IPC_INIT_REF_FILE
    export TDAQ_IPC_INIT_REF="file:$IPC_INIT_REF_FILE"
    echo "done."
}

function setup {
    if [ $SETUP_PERFORMED -eq 1 ]; then return; fi
    SETUP_PERFORMED=1

    setup_env

# Make sure that the cleanup function called will be called before exiting.
    trap cleanup 0

    echo "*** Starting servers ... "
    start_server "ipc_server" "test_ipc_server -p initial"
    start_server "ipc_server -p $TDAQ_PARTITION" 
    start_server "is_server  -p $TDAQ_PARTITION -n DF -t -1" \
                 "test_is_server -p $TDAQ_PARTITION -n DF" 
    echo "*** done."
}


# Run the tests keeping track of whether there were any failures.
EXIT_CODE=0
while (test $# -gt 0); do
    case "$1" in
        -s)
        shift
        test $# -ne 0 && SLEEP_TIME=$1
        setup
        echo "Keeping servers running for " $SLEEP_TIME " seconds"
        sleep $SLEEP_TIME
        break
        ;;

        -bin)
        shift
        BIN_PATH=$1
        shift
        ;;

        *)
        setup
        TEST_APPLICATION=$1
        echo -n "*** $TEST_APPLICATION: "
        $BIN_PATH/$TEST_APPLICATION
        test $? -ne 0 && EXIT_CODE=1
        shift
        ;;
    esac 
done

exit $EXIT_CODE


#ifndef TEST_ONLINE_FIXTURE_H_
#define TEST_ONLINE_FIXTURE_H_

#include <ipc/core.h>
#include <ipc/partition.h>
#include <is/infodictionary.h>

/*
 * Test fixture which sets up a partition.
 */
class OnlineTestFixture {
public:
    static std::string& get_app_name()
    {
        static std::string app_name(getenv("TDAQ_APPLICATION_NAME"));
        return app_name;
    }

    static std::string& get_server_name()
    {
        static std::string server_name(getenv("TDAQ_IS_SERVER_NAME"));
        return server_name;
    }

    static IPCPartition& get_partition()
    {
        static IPCPartition partition(getenv("TDAQ_PARTITION"));
        static boost::once_flag flag = BOOST_ONCE_INIT;
        boost::call_once(&OnlineTestFixture::init, flag);

        return partition;
    }

    static ISInfoDictionary& get_is_dictionary()
    {
        static ISInfoDictionary dict(get_partition());
        return dict;
    }

    static void init() {
        static int argc = 1;
        static const char *argv = "test_config";
        IPCCore::init(argc, const_cast<char **>(&argv));
    }
};


#endif /* TEST_ONLINE_FIXTURE_H_ */

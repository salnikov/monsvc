#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE monsvc Commander Tests
#define BOOST_TEST_MAIN

#include <boost/test/unit_test.hpp>

#include <oh/OHRootReceiver.h>
#include <oh/OHCommandSender.h>

#include <TH1F.h>

#include <iostream>
#include <cstdio>

#include "monsvc/PublishingController.h"
#include "monsvc/MonitoringService.h"
#include "monsvc/ConfigurationRules.h"
#include "monsvc/detail/OHPublisher.h"
#include "monsvc/Exceptions.h"
#include "monsvc/NameFilter.h"

#include "monsvc/detail/Registry.h"

#include "test/unit/test_online_fixture.h"

class CommanderTestFixture : public OnlineTestFixture {
public:
    ~CommanderTestFixture() {
        monsvc::detail::Registry::instance().clear();
    }
};

BOOST_FIXTURE_TEST_SUITE(CommanderTest, CommanderTestFixture)

BOOST_AUTO_TEST_CASE(check_oh_command)
{
    const std::string provider_name = "provider_name";
    const std::string object_name = "oh_object_name";
    const std::string notmatch_name = "not_matching";

    // Create the publisher by using a rule.
    std::shared_ptr<monsvc::ConfigurationRule> rule = 
      monsvc::ConfigurationRule::from(
                                      "rule:.*/=>oh:(2,1," + get_server_name() +
                                      "," + provider_name + ")");

    // Check that the publisher has the right type.
    std::shared_ptr<monsvc::PublisherBase> pub =
            rule->create_publisher(get_partition(), get_app_name());

    // Register the objects in the registry
    TH1F histo("histo","histo", 100, 0., 100.);
    monsvc::MonitoringService::instance().register_object(object_name, &histo);
    TH1F histo1("histo1","histo1", 100, 0., 100.);
    monsvc::MonitoringService::instance().register_object(notmatch_name, 
                                                          &histo1);
    
    monsvc::Tag nulltag = monsvc::Tag();
    monsvc::Annotations ann;
    // Publish a histogram.
    histo.Fill(50.);
    pub->publish(object_name, nulltag, ann, &histo);
    histo1.Fill(50.);
    pub->publish(notmatch_name, nulltag, ann, &histo1);
    
    // And check that we can retrieve it from the OH server.
    OHRootHistogram recvd_histo = OHRootReceiver::getRootHistogram(
            get_partition(), get_server_name(), provider_name, object_name);
    BOOST_CHECK_EQUAL(recvd_histo.getObject()->GetMaximumBin(), 51);
    

    // Send a command
    OHCommandSender cmder(get_partition());
    cmder.sendCommand(get_server_name(), provider_name, object_name, "reset");

    pub->publish(object_name, nulltag, ann, &histo);

    //Check the target was reset
    recvd_histo = 
      OHRootReceiver::getRootHistogram(get_partition(), get_server_name(), 
                                       provider_name, object_name);
    BOOST_CHECK_EQUAL(recvd_histo.getObject()->GetBinContent(51),0);
    //Check we reset only one histo
    recvd_histo = 
      OHRootReceiver::getRootHistogram(get_partition(), get_server_name(), 
                                       provider_name, notmatch_name);
    BOOST_CHECK_EQUAL(recvd_histo.getObject()->GetMaximumBin(), 51);

    // Send a non existing command
    cmder.sendCommand(get_server_name(), provider_name, object_name, "what's this?");
    
    //Refill ands re-publish
    histo.Fill(50.);
    pub->publish(object_name, nulltag, ann, &histo);
    
    //Reset the whole provider
    cmder.sendCommand(get_server_name(), provider_name, "reset");
    
    pub->publish(object_name, nulltag, ann, &histo);
    pub->publish(notmatch_name, nulltag, ann, &histo1);

    //Check all histos were reset
    recvd_histo = 
      OHRootReceiver::getRootHistogram(get_partition(), get_server_name(), 
                                       provider_name, object_name);
    BOOST_CHECK_EQUAL(recvd_histo.getObject()->GetBinContent(51),0);
    recvd_histo = 
      OHRootReceiver::getRootHistogram(get_partition(), get_server_name(), 
                                       provider_name, notmatch_name);
    BOOST_CHECK_EQUAL(recvd_histo.getObject()->GetBinContent(51),0);

    //Re-publish histograms
    histo.Fill(50.);
    pub->publish(object_name, nulltag, ann, &histo);
    histo1.Fill(50.);
    pub->publish(notmatch_name, nulltag, ann, &histo1);
  
    //Reset with regex
    cmder.sendCommand(get_server_name(), provider_name, "reset;oh_.*");
    
    pub->publish(object_name, nulltag, ann, &histo);
    pub->publish(notmatch_name, nulltag, ann, &histo1);

    //Check the target was reset
    recvd_histo = 
      OHRootReceiver::getRootHistogram(get_partition(), get_server_name(), 
                                       provider_name, object_name);
    BOOST_CHECK_EQUAL(recvd_histo.getObject()->GetBinContent(51),0);
    //Check we reset only one histo
    recvd_histo = 
      OHRootReceiver::getRootHistogram(get_partition(), get_server_name(), 
                                       provider_name, notmatch_name);
    BOOST_CHECK_EQUAL(recvd_histo.getObject()->GetMaximumBin(), 51);

    //Send malformed regex
    cmder.sendCommand(get_server_name(), provider_name, "reset;oh_.* what is this)");
}

BOOST_AUTO_TEST_SUITE_END()

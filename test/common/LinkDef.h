#ifndef MONSVC_LINKDEF_H_
#define MONSVC_LINKDEF_H_

#include "test_monsvc.h"

#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ class MyObject;
#endif


#endif // MONSVC_LINKDEF_H_

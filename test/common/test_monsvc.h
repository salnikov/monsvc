// -*- c++ -*-
#ifndef MONSVC_TEST_H_
#define MONSVC_TEST_H_

#include <TObject.h>

#include <string>
#include <vector>


//
// A class that derives from TObject
//
class MyObject : public TObject {
public:
    MyObject(int x, float y, double d, int a_fill, float b_fill, std::vector<double> c,
            std::string s) : x(x), y(y), d(d), c(c), s(s) {
        for(int i = 0; i< 10; i++) a[i] = a_fill * i;
        for(int i = 0; i< 20; i++) b[i] = b_fill * i;
    }

    MyObject()
        : x(5),
          y(2.7),
          d(3.141),
          c(20, 1.1),
          s("Hello World")
    {
        for(int i = 0; i< 10; i++) a[i] = i;
        for(int i = 0; i< 20; i++) b[i] = i * 2.3;
    }

    void Print(const Option_t * = "") const;

    void f() {}

    virtual ~MyObject() {}

public:
    int x;     // x
    float y;   // y
    double d;  // d

    int a[10];
    float b[20];

    std::vector<double> c;

    std::string s;

    // for rootcint
    ClassDef(MyObject, 1);
};


#endif // MONSVC_TEST_H_

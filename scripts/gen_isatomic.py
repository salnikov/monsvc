#!/usr/bin/env tdaq_python
"""
Generate an C++ declaration for an IS object, using
atomic<> for basic discrete types.

Usage: gen_isatomic.py [-d <output_directory>] [-p <namespace>] [-c <class_name>]* <is.schema.xml>

"""

import config
import string
import sys
from getopt import getopt

atomic_types = [ 'bool',
                 'u8', 'u16', 'u32', 'u64',
                 's8', 's16', 's32', 's64' ]

type_map = {
    'bool' : 'tbb::atomic<bool>',
    'u8'   : 'tbb::atomic<uint8_t>',
    'u16'  : 'tbb::atomic<uint16_t>',
    'u32'  : 'tbb::atomic<uint32_t>',
    'u64'  : 'tbb::atomic<uint64_t>',
    's8'   : 'tbb::atomic<int8_t>',
    's16'  : 'tbb::atomic<int16_t>',
    's32'  : 'tbb::atomic<int32_t>',
    's64'  : 'tbb::atomic<int64_t>',
    'string' : 'std::string',
    'time' : 'OWLTime',
    'date' : 'OWLDate'
    }

type_map_no_atomic = {
    'u8'  : 'uint8_t',
    'u16' : 'uint16_t',
    'u32' : 'uint32_t',
    'u64' : 'uint64_t',
    's8'  : 'int8_t',
    's16' : 'int16_t',
    's32' : 'int32_t',
    's64' : 'int64_t',
    'string' : 'std::string'
    }

def get_value(name, attr):
    if attr['type'] in atomic_types:
        return '(' + type_map[attr['type']] + ' )' + name 
    else:
        return name

def set_value(name, attr):
    if attr['type'] in atomic_types:
        return '(' + type_map_no_atomic[attr['type']] + '& )' + name
    else:
        return name

def gen_class(f, name, attrs, data, namespace):
    
    print >>f,"#ifndef GEN_" + name +'_H_'
    print >>f,'#define GEN_' + name +'_H_'

    print >>f
    print >>f,'#include <tbb/atomic.h>'
    print >>f,'#include <is/info.h>'
    print >>f

    print >>f,'namespace ' + namespace + ' {'
    print >>f
    
    print >>f,'  class ' + name + ' : public ISInfo {'
    print >>f,'  public:'

    print >>f,'   ',name,'()'
    print >>f,'     : ISInfo("' + name + '"),\n      ',string.join([a + '()' for a in attrs], ',\n       ')
    print >>f,'    {}'
    print >>f

    for a in attrs:
        if data[a]['multivalue']:
            t = type_map_no_atomic.get(data[a]['type'],data[a]['type'])
            print >>f, '    std::vector<' + t + '> ' + a + ';'
        else:
            t = type_map.get(data[a]['type'],data[a]['type'])
            print >>f,'    ' + t + '  ' + a + ';'
            if not data[a]['type'] in atomic_types:
                print >>f, '    void set_' + a + '(' + t + ' arg) { ' + a + ' = arg; }'
                print >>f, '    ' + t + ' get_' + a + '() const { return ' + a + '; }'
                if data[a]['type'] in [ 'float', 'double' ]:
                    print >>f, '    void add_' + a + '(' + t + ' arg) { ' + a + ' += arg; }'
                print >>f

    print >>f
    print >>f,'    void publishGuts(ISostream& s) {'
    print >>f,'       s << ',
    print >>f,string.join(attrs,'\n         << '),';'
    print >>f,'    }'
    print >>f
    
    print >>f,'    void refreshGuts(ISistream& s) {'
    print >>f,'       s >> ',
    print >>f,string.join([set_value(a,data[a]) for a in attrs],'\n         >> '),';'    
    print >>f,'    }'

    print >>f,'    std::ostream& print(std::ostream& os) const {'
    print >>f,'        os <<',
    print >>f,string.join([ '\"  ' + a + ':\\t\" << ' + a + ' << \'\\n\'' for a in attrs], '\n           << ')
    print >>f,'           << std::endl;'
    print >>f,'        return os;'
    print >>f,'    }'
        
    print >>f,'  };'
    print >>f,'}'

    print >>f, 'inline std::ostream& operator<<(std::ostream& os, const ' + namespace + '::' + name + '& info) { info.print(os); return os; }'
    print >>f
    print >>f,'#endif // GEN_' + name + '_H_'


optlist, args = getopt(sys.argv[1:],'p:c:d:')

namespace = 'daq'
classes = []
output_dir = ''

for opt,arg in optlist:
    if opt == '-n':
        namespace = arg
    elif opt == '-c':
        classes.append(arg)
    elif opt == '-d':
        output_dir = arg
        if output_dir[-1] != '/':
            output_dir += '/'


c = config.Configuration('oksconfig:' + args[0])

for cl in classes:
    attrs = [ a.split(':')[0] for a in super(config.Configuration, c).attributes(cl, False)]
    data = c.attributes(cl)
    f = file(output_dir + cl + '.h','w')    
    gen_class(f, cl, attrs, data, namespace)

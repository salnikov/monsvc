/*
 * Exceptions.h
 *
 *  Created on: Oct 31, 2012
 *      Author: ctalau
 */

#ifndef MONSVC_EXCEPTIONS_H_
#define MONSVC_EXCEPTIONS_H_

#include <ers/ers.h>
#include <boost/date_time/posix_time/posix_time.hpp>

/**
 * @brief Base class for all configuration-related issues.
 */
ERS_DECLARE_ISSUE(monsvc, ConfigurationIssue, ERS_EMPTY, ERS_EMPTY)

ERS_DECLARE_ISSUE_BASE( monsvc , BadRuleFormat , ConfigurationIssue ,
        "The format of the rule \"" << rule << "\" is wrong! Reason: " << reason ,
        ERS_EMPTY,
        ((std::string) rule) ((std::string) reason))

ERS_DECLARE_ISSUE_BASE( monsvc , BadRuleType , ConfigurationIssue ,
        "The rule " << name << " is neither for IS nor for OH.",
        ERS_EMPTY,
        ((std::string) name) ((std::string) type))

ERS_DECLARE_ISSUE_BASE( monsvc , InvalidInterval , ConfigurationIssue ,
        "Rule " << rule_name << " has interval 0 and will not be used.",
        ERS_EMPTY,
        ((std::string) rule_name))

ERS_DECLARE_ISSUE_BASE( monsvc , CannotCreatePublisher , ConfigurationIssue ,
        "Cannot create " << type << " publisher.",
        ERS_EMPTY,
        ((std::string) type))

ERS_DECLARE_ISSUE_BASE( monsvc , ApplicationNotFound , ConfigurationIssue ,
        "Cannot find application: " << application,
        ERS_EMPTY,
        ((std::string) application))

ERS_DECLARE_ISSUE_BASE( monsvc , InvalidCommandRunning , ConfigurationIssue ,
        "Invalid command for current state: already running.",
        ERS_EMPTY,
        ERS_EMPTY)

ERS_DECLARE_ISSUE_BASE( monsvc , InvalidCommandStopped , ConfigurationIssue ,
        "Invalid command for current state: already stopped.",
        ERS_EMPTY,
        ERS_EMPTY)

ERS_DECLARE_ISSUE_BASE( monsvc , InvalidAddRuleRunning, ConfigurationIssue ,
        "Cannot add new publishing rule while running, rule=\"" << rule << "\"",
        ERS_EMPTY,
        ((std::string) rule))

/**
 * @brief Base class for all publishing related issues.
 */
ERS_DECLARE_ISSUE( monsvc , PublishingIssue , ERS_EMPTY, ERS_EMPTY)

ERS_DECLARE_ISSUE_BASE( monsvc , ISPublishingIssue , PublishingIssue,
        "Error while publishing object " << name,
        ERS_EMPTY,
        ((std::string) name))

ERS_DECLARE_ISSUE_BASE( monsvc , OHPublishingIssue , PublishingIssue,
        "Error while publishing object " << name,
        ERS_EMPTY,
        ((std::string) name))

ERS_DECLARE_ISSUE_BASE( monsvc , OHPublishingInvalidType , PublishingIssue,
        "Object " << name << " cannot be published as it cannot be casted to a valid type",
        ERS_EMPTY,
        ((std::string) name))

ERS_DECLARE_ISSUE_BASE( monsvc , CustomOstreamPublishingIssue , PublishingIssue,
        "The TObject objects will be printed to std::cout, even if a different output stream was "
        "provided ",
        ERS_EMPTY,
        ERS_EMPTY)

ERS_DECLARE_ISSUE_BASE( monsvc , UnexpectedPublishingIssue , PublishingIssue,
        "Unexpected publishing error! If not ignored, this would have killed the current thread.",
        ERS_EMPTY,
        ERS_EMPTY)

ERS_DECLARE_ISSUE_BASE( monsvc , PublishingDeadlineMissedIssue , PublishingIssue,
        "We missed the " << limit << " publishing deadline by " << excess_time ,
        ERS_EMPTY,
        ((boost::posix_time::time_duration) limit) ((boost::posix_time::time_duration) excess_time))

/**
 * @brief Base class for all issues related to object registration.
 */
ERS_DECLARE_ISSUE( monsvc , RegistryIssue , ERS_EMPTY, ERS_EMPTY)

ERS_DECLARE_ISSUE_BASE( monsvc , WrongType, RegistryIssue,
        "Bad type for object " << name ,
        ERS_EMPTY,
        ((std::string) name))

ERS_DECLARE_ISSUE_BASE( monsvc , NotFound, RegistryIssue,
        "Object \"" << name << "\" and tag \"" << tag << "\" not found",
        ERS_EMPTY,
        ((std::string) name) ((std::string) tag))

ERS_DECLARE_ISSUE_BASE( monsvc , NameExists, RegistryIssue,
        "Another object with name \"" << name << 
                        "\" and tag \""<< tag << "\" exists",
        ERS_EMPTY,
        ((std::string) name) ((std::string) tag))

/**
 * @brief Base class for all issues related to OH commands
 */
ERS_DECLARE_ISSUE( monsvc , OHCommandIssue , ERS_EMPTY, ERS_EMPTY)

ERS_DECLARE_ISSUE_BASE( monsvc , HistogramNotFound, OHCommandIssue,
                        "Histogram not found " << name ,
                        ERS_EMPTY,
                        ((std::string) name))

ERS_DECLARE_ISSUE_BASE( monsvc , UnknownCommand, OHCommandIssue,
                        "Unknown command: " << name ,
                        ERS_EMPTY,
                        ((std::string) name))

/**
 * @brief Base class for all issues related to regex
 */
ERS_DECLARE_ISSUE( monsvc , RegExIssue , ERS_EMPTY, ERS_EMPTY)

ERS_DECLARE_ISSUE_BASE( monsvc , MalformedRegEx, RegExIssue,
                        "Malformed regex detected: '" << name << "'. " << 
                        action,
                        ERS_EMPTY,
                        ((std::string) name) ((std::string)action))


#endif /* EXCEPTIONS_H_ */

// -*- c++ -*-
#ifndef MONSVC_PTR_H_
#define MONSVC_PTR_H_

#include "monsvc/descriptor.h"

#include <memory>
#include <mutex>

namespace monsvc {
    // Forward declaration of the Registry class.
    namespace detail {
        class Registry;
    }

    /**
     * @brief Smart pointer class to interact with registered objects in a thread-safe way.
     *
     * A user registers an object to be published through the
     * MonitoringService::register_object() method under a given name.
     *
     * @code
     *    TH1F *h = new TH1F(...)
     *    ptr<TH1F> histo = MonitoringService::instance().register_object("test", h);
     * @endcode
     *
     * In return he gets a ptr<T> object that provides access
     * to the object.
     *
     * Calling methods through the ptr<T>  locks the underlying
     * object and releases the lock after the method call.
     *
     * @code
     *    hist->Fill(100); // thread safe
     * @endcode
     *
     * The ptr<T> object itself implements the Lockable concept.
     * It can be used with any boost or std lock or mutex (but
     * not timed locks):
     *
     * @code
     *    boost::unique_lock<ptr<TH1F> > lock(histo);
     * @endcode
     *
     * or
     *
     * @code
     *    ptr<TH1F>::scoped_lock lock(histo);
     * @endcode
     *
     *
     * Now use the original pointer
     *
     * @code
     *    h->Fill(100);
     * @endcode
     *
     * or get underlying raw object
     *
     * @code
     *    TH1F *h2 = hist->get();
     *    h2->Fill(100);
     * @endcode
     *
     */

    template<class T>
    class ptr
    {
    public:
        /** @brief A lock_guard that uses a ptr as a Lockable object. */
        typedef std::lock_guard<ptr> scoped_lock;

        ptr() {}
        ~ptr() {}

        /** @brief The lock method of the Lockable concept. */
        void lock()     { m_obj->lock(); }
        /** @brief The unlock method of the Lockable concept. */
        void unlock()   { m_obj->unlock(); }
        /** @brief The try_lock method of the Lockable concept. */
        bool try_lock() { return m_obj->try_lock(); }

        /**
         * @brief Obtain the raw pointer.
         *
         * While accessing the pointer, one should hold the lock the \ref ptr object, or
         * make sure by other means that no publisher is executing at the same time.
         */
        T *get() { return m_obj.get()->object(); }

        /** @brief Obtain the @c const version of the raw pointer. */
        const T *get() const { return m_obj.get()->object(); }

    private:
        friend class MonitoringService;
        explicit ptr(std::shared_ptr<descriptor<T> > obj) : m_obj(obj) {}

        typedef typename std::shared_ptr<descriptor<T> > rep_type;

        class locker {
        public:
            locker(rep_type p) : m_ptr(p) { m_ptr->lock(); }
            ~locker() { m_ptr->unlock(); }
            T* operator->() { return m_ptr.get()->object(); }
            const T* operator->() const { return m_ptr.get()->object(); }

            T& operator*() { return *m_ptr.get()->object(); }
            const T& operator*() const { return *m_ptr.get()->object(); }

        private:
            rep_type m_ptr;
        };

    public:
        /**
         * @brief Dereferences the smart pointer.
         *
         * While the dereferenced object is in scope, the lock is held, so no other thread
         * can concurrently access it. Thus, one should minimize this scope.
         */
        locker operator->() { return locker(m_obj); }

        /**
         * @brief Const version of the dereference operator.
         *
         * @sa operator->()
         */
        const locker operator->() const { return locker(m_obj); }

        /**
         * @brief The dereference operator.
         *
         * @sa operator->()
         */
        locker operator*() { return locker(m_obj); }

        /**
         * @brief Const version of the dereference operator.
         *
         * @sa operator->()
         */
        const locker operator*() const { return locker(m_obj); }

    private:
        rep_type m_obj;
    };

}

#endif // MONSVC_PTR_H_

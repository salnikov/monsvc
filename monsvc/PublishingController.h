/*
 * OnlineMonitoringConfigurator.h
 *
 *  Created on: Oct 24, 2012
 *      Author: ctalau
 */

#ifndef MONSVC_ONLINE_MONITORING_CONFIGURATOR_H_
#define MONSVC_ONLINE_MONITORING_CONFIGURATOR_H_

#include "monsvc/ConfigurationRules.h"
#include "monsvcdal/PublishingApplication.h"

#include "boost/utility.hpp"
#include "boost/property_tree/ptree_fwd.hpp"

class IPCPartition;
class Configuration;

namespace monsvc {
  
   class PublisherBase;
  
    /**
     * @brief Controller for the online publishing.
     *
     * This class is used to configure the publishing of monitoring information to the
     * online services (IS & OH). It uses a set of rules which are either added programmatically
     * or loaded from an OKS database.
     *
     * When the publishing is started all the configuration rules that are already added are used.
     * New rules take place immediately after they are added. Publishing can be re-started after
     * it was stopped.
     *
     * The class is thread safe.
     */
    class PublishingController : private boost::noncopyable {
    public:
        PublishingController();
        ~PublishingController();

        /**
         * @brief Crates an OnlineMonitoringConfiguration for the current application.
         *
         * @param partition the IPC partition in which the current application runs.
         * @param app_name the name of the current application.
         */
        PublishingController(const IPCPartition& partition, const std::string& app_name);

        /**
         * @brief Adds a new configuration rule to the configuration.
         *
         * If another rule with the same name exists, the old one is disabled.
         */
        void add_configuration_rule(const ConfigurationRule& rule);

        /**
         * @brief Disable one of the existing rules.
         */
        void disable_configuration_rule(const std::string& rule_name);

        /**
         * @brief Adds all the rules present in the OKS database associated 
         * with the object with UID equal to the application name provided in 
         * the constructor. Beware of template applications!.
         */
        void add_configuration_rules(Configuration& conf);

        /**
         * @brief Adds all the rules present in the OKS database.
         */
        void add_configuration_rules(Configuration& conf, const dal::PublishingApplication* app);

        /**
         * @brief Adds all the rules from a boost::ptree object. This is 
         * expected to map to the OKS configuration scheme. The top object
         * must represent a ConfigurationRuleBundle-like structure
         * **ConfigurationRuleBundle**
         *   UID : xxxx
         *   LinkedBundles:
         *      ConfigurationRuleBundle:
         *         ...
         *         ...
         *      <more ConfigurationRuleBundle here>
         *   Rules:
         *      ConfigurationRule:
         *         ...
         *         ...
         *      <more ConfigurationRule here>
         */
        void add_configuration_rules(const boost::property_tree::ptree &bundle);

        /**
         * @brief Adds one rule from a boost::ptree object. This is 
         * expected to map to the OKS configuration scheme. The top object
         * must represent a ConfigurationRule-like structure
         * **ConfigurationRule**
         *  UID : string
         *  IncludeFilter : regex
         *  ExcludeFilter : regex
         *  Name : string
         *  Parameters :
         *     OHPublishingParameters [ISPublishingParameters]:
         *        UID : string
         *        PublishInterval : seconds
         *        OHServer : string
         *        ROOTProvider : string
         */
        void add_configuration_rule(const boost::property_tree::ptree &rule);

        /**
         * @brief Returns a vector with string representations of the rules.
         */
        const std::vector<std::string> get_rules() const;

        /**
         * @brief Starts publishing monitoring information.
         */
        void start_publishing();

        /**
         * @brief Stops publishing monitoring information.
         */
        void stop_publishing();

        /**
         * @brief Forces an immediate publication of all registered rules
         */
        void publish_all();

    private:
        class Impl;
        std::unique_ptr<Impl> m_impl;
    };

} /* namespace monsvc */
#endif /* MONSVC_ONLINE_MONITORING_CONFIGURATOR_H_ */

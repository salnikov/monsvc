// this is -*- c++ -*-
#ifndef MONSVC_PUBLISHER_H_
#define MONSVC_PUBLISHER_H_

#include "monsvc/Annotation.h"

#include <string>

class TObject;
class TH1;
class TGraph;
class TGraph2D;
class ISInfo;
namespace monsvc {
  class Tag;
}

namespace monsvc {
    /**
     * @brief The base class for visitors of the Registry.
     *
     * A new publishing implementation will have to
     * inherit at some point from this class and implement
     * one or more of the virtual functions.
     *
     * "knows" functions are used to inform the caller of the
     * capabilities of the subclasses. They allow to take actions
     * only if the visitor will actually perform something on the object.
     */
    class PublisherBase {
    public:
        PublisherBase();
        virtual ~PublisherBase();

        /**
         * @brief Can publish the TObject object.
         *
         * Should be implemented by subclasses. Does nothing by default.
         */
        virtual bool knows(TObject *) { return false; }

        /**
         * @brief Publishes the TObject object.
         *
         * Should be implemented by subclasses. Does nothing by default.
         */
        virtual void publish(const std::string&, const Tag&,
                             const Annotations&, TObject *) {}

        /**
         * @brief Can publish the TH1 object.
         *
         * Should be implemented by subclasses. Does nothing by default.
         */
        virtual bool knows(TH1 *) { return false; }

        /**
         * @brief Publishes the TH1 object.
         *
         * Should be implemented by subclasses. Does nothing by default.
         */
        virtual void publish(const std::string&, const Tag&,
                             const Annotations&, TH1 *) {}

        /**
         * @brief Can publish the TGraph object.
         *
         * Should be implemented by subclasses. Does nothing by default.
         */
        virtual bool knows(TGraph *) { return false; }

        /**
         * @brief Publishes the TGraph object.
         *
         * Should be implemented by subclasses. Does nothing by default.
         */
        virtual void publish(const std::string&, const Tag&,
                             const Annotations&, TGraph *) {}

        /**
         * @brief Can publish the TGraph2D object.
         *
         * Should be implemented by subclasses. Does nothing by default.
         */
        virtual bool knows(TGraph2D *) { return false; }
        
        /**
         * @brief Publishes the TGraph2D object.
         *
         * Should be implemented by subclasses. Does nothing by default.
         */
        virtual void publish(const std::string&, const Tag&,
                             const Annotations&, TGraph2D *) {}

        /**
         * @brief Can publish the TGraph2D object.
         *
         * Should be implemented by subclasses. Does nothing by default.
         */
        virtual bool knows(ISInfo *) { return false; }

        /**
         * @brief Publishes the ISInfo object.
         *
         * Should be implemented by subclasses. Does nothing by default.
         */
        virtual void publish(const std::string&,  const Tag&,
                             const Annotations&, ISInfo *) {}
    };
}

#endif // MONSVC_PUBLISHER_H_

/* Local Variables: */
/* c-basic-offset:4 */
/* End: */

/*
 * OHPublisher.h
 *
 *  Created on: Oct 19, 2012
 *      Author: ctalau
 */

#ifndef MONSVC_OHPUBLISHER_H_
#define MONSVC_OHPUBLISHER_H_

#include "monsvc/PublisherBase.h"
#include <oh/OHRootProvider.h>

#include <memory>

class OHRootProvider;
class IPCPartition;

namespace monsvc {

    /**
     * @brief PublisherBase implementation that publishes information to an OH server.
     *
     * It publishes only TH1, TGraph and TGraph2D objects.
     */
    class OHPublisher : public PublisherBase {
    public:
        /**
         * @brief Creates an OHPublisher.
         *
         * @param partition the IPC partition in which the current application runs.
         * @param server_name the name of the OH server.
         * @param provider_name the provider name to be used when publishing.
         * @param cmdlistener the command listener instance for this provider
         */
        OHPublisher(const IPCPartition& partition, 
                    const std::string& server_name,
                    const std::string& provider_name,
                    std::shared_ptr<OHCommandListener> cmdlistener = std::shared_ptr<OHCommandListener>());
        virtual ~OHPublisher();

        using PublisherBase::publish;
        using PublisherBase::knows;
       
        /** @brief Publishes a TObject to the OH server, if it matches 
            one of the valid types */
        virtual void publish(const std::string&, const Tag&,
                             const Annotations&, TObject *) override;
        virtual bool knows(TObject *) override { return true; }
       
        /** @brief Publishes the TH1 histogram to the OH server */
        virtual void publish(const std::string&, const Tag&,
                             const Annotations&, TH1 *) override;
        virtual bool knows(TH1 *) override { return true; }
        
        /** @brief Publishes the TGraph object to the OH server */
        virtual void publish(const std::string&, const Tag&,
                             const Annotations&, TGraph *) override ;
        virtual bool knows(TGraph *) override { return true; }
        
        /** @brief Publishes the TGraph2D object to the OH server */
        virtual void publish(const std::string&, const Tag&,
                             const Annotations&, TGraph2D *) override;
        virtual bool knows(TGraph2D *) override { return true; }

    private:
        template <typename T> void 
          do_publish(const std::string&, const Tag&, const Annotations&, T*);
        std::shared_ptr<OHRootProvider> m_provider;
        std::shared_ptr<OHCommandListener> m_cmdlistener;
    };

} /* namespace monsvc */
#endif /* MONSVC_OHPUBLISHER_H_ */

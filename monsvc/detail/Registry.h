// this is -*- c++ -*-
#ifndef MONSVC_REGISTRY_H_
#define MONSVC_REGISTRY_H_

#include "monsvc/Exceptions.h"
#include "monsvc/descriptor.h"

#include <memory>
#include <map>
#include <mutex>

namespace monsvc {

    namespace detail {

        /*
         * @brief The Registry used to store the monitored objects.
         *
         * The for_each() method can be used to iterate over the
         * entries in the Registry. This is mainly for Publisher
         * implementations.
         *
         * The Registry can be reset by calling clear().
         *
         */
        class Registry {
        private:
            Registry();
            
            Registry(const Registry& ); //  = delete;
            Registry& operator=(const Registry& ); // = delete;
            
        public:
            /**
             * @brief An entry in the Registry.
             *
             * It contains a pointer to the object, and other bookkeeping data.
             * @sa descriptor_base.
             */
            typedef std::shared_ptr<descriptor_base> Entry;

            /** @brief Weak reference to a Registry entry. */
            typedef std::weak_ptr<descriptor_base> EntryRef;

            /**
             * @brief Publish object under name and tag.
             *
             * Optionally a callback can be provided to fill the object just before publishing.
             * The object will be locked at the time the callback is executing.
             * Optionally the Registry can take ownership of the object and delete it when
             * all references have gone.
             */
            void publish_object(const std::string& name,
                                Registry::Entry rep);

            /**
             * @brief Find an object with the given name and tag.
             *
             * throws:
             *   WrongType
             *   NotFound
            */
            Registry::Entry
            find_object(const std::string& name, const Tag& tag) const;

            /**
             * @brief Find all objects with the given name.
             *
             * throws:
             *   WrongType
             *   NotFound
            */
            std::vector<Registry::Entry> 
            find_objects(const std::string& name) const;
            
            /**
             * @brief Remove the object with the given name and tag 
             * from publishing.
             */
            bool remove_object(const std::string& name, const Tag& tag);

            /**
             * @brief Remove all object with the given name
             */
            int remove_objects(const std::string& name);
            
            /**
             * @brief Remove the first descriptor that satisfies the 
             * predicate @c p.
             */
            bool remove_if(std::function<bool(Registry::Entry)> p);

            /**
             * @brief Iterate over all entries in the registry.
             *
             * Pass to the callback an object of type Registry::Entry which is really a
             * shared_ptr<descriptor_base>.
             *
             * The signature of F should be:
             *
             * void f(monsvc::Registry::Entry obj);
             *
             */
            void for_each(std::function<void(const Entry)> f) const;

            /// @brief Remove all entries, delete objects for which we have ownership.
            void clear();

            /// @brief Get the unique instance of the Registry.
            static Registry& instance();

        private:
            typedef std::multimap<std::string, std::shared_ptr<descriptor_base> > Storage;
            mutable std::mutex m_mutex;
            Storage m_objects;
        };
    }
}

#endif // MONSVC_REGISTRY_H_

/* Local Variables: */
/* c-basic-offset:4 */
/* End: */

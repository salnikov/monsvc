// -*- c++ -*-
#ifndef MONSVC_TINFO_H_
#define MONSVC_TINFO_H_

#include <is/info.h>
#include <TObject.h>

namespace monsvc {
    namespace detail {
        /*
         * @brief A helper class to publish an arbitrary ROOT TObject into IS.
         *
         * This requires that the underlying class has the necessary
         * ROOT metadata available.
         *
         * Restrictions in the current version:
         *
         *   - only basic types can be published.
         *   - arrays are not supported.
         *   - strings are not supported except for 'char *'
         *   - other STL containers are not supported.
         */
        class TInfo : public ISInfo {
        public:
            /**
             * @brief Creates a TInfo wrapper for the given TObject.
             */
            TInfo(TObject *obj);

            /**
             * @brief Publishes the object to the ISostream.
             */
            void publishGuts(ISostream& s);

            /**
             * @brief This function is not used and not implemented.
             */
            void refreshGuts(ISistream& s);
        private:
            TObject *m_obj;
        };
    }

}

#endif // MONSVC_TINFO_H_

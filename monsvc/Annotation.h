// -*- c++ -*-
#ifndef MONSVC_ANNOTATION_H_
#define MONSVC_ANNOTATION_H_

#include <vector>
#include <string>

namespace monsvc {
  typedef std::pair<std::string, std::string> Annotation;
  typedef std::vector<Annotation> Annotations;

  const Annotation FINALPUBLICATION = {"FINAL", "FINAL"};

}
#endif  // MONSVC_ANNOTATION_H_

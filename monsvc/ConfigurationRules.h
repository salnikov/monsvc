/*
 * ConfigurationRules.h
 *
 *  Created on: Oct 26, 2012
 *      Author: ctalau
 */

#ifndef MONSVC_CONFIGURATION_RULES_H_
#define MONSVC_CONFIGURATION_RULES_H_

#include "monsvc/Exceptions.h"
#include "monsvc/NameFilter.h"
#include "monsvcdal/ConfigurationRule.h"

#include <memory>
#include "boost/property_tree/ptree_fwd.hpp"

class IPCPartition;
class Configuration;

namespace monsvc {

  class PublisherBase;

    /**
     * @brief Base class for the publishing configuration rules.
     *
     * The publishing of the monitored objects is governed by a set of named configuration rules.
     * Each rule specify the set of the objects it applies to by using a NameFilter. A rule also
     * specifies a set of publishing parameters like: the server to which to send the objects,
     * the name of the file, etc.
     */
    class ConfigurationRule {
    public:
        /**
         * @brief Creates a publisher which publishes according to the parameters of the rule.
         */
        virtual std::shared_ptr<PublisherBase> create_publisher(const IPCPartition& partition,
                const std::string& app_name) const = 0;

        /**
         * @brief Builds a configuration rule from a string specification.
         *
         * The format of the rule should have one of the following formats:
         *
         *  - "<name>:<include_filter>/<exclude_filter>=><type>:(<parameters>)"
         *
         * where:
         *
         *  - <name> is the name of the rule
         *  - <include_filter> and <exclude_filter> are regular expressions
         *  - <type> is the type of the publisher: 'is' or 'oh'
         *  - <parameters> are specific to the publisher type, and comma separated.
         *    is --> (<interval (s)>,<n slots>,<server name>)
         *    oh --> (<interval (s)>,<n slots>,<server name>,<provider name>)
         *
         * The provided server name and provider name can either be:
         * - a plain string
         * - a string in the form ${<VARIABLE_NAME>[=<DEFAULT_SERVER_NAME>]}[<SUFFIX>]. 
         * In this case, the returned name will be the value of the 
         * environmental variable VARIABLE_NAME, if it exists, otherwise the
         * default value DEFAULT_SERVER_NAME, if provided. In case none of the 
         * two is met, the function will throw. If SUFFIX is provided, the value
         * is appended to the above result.
         *
         * If the number of slots is not provided, the default is 1
         *
         *  @throws BadRuleFormat if the rule does not comply with the required format.
         *  @throws boost::bad_lexical_cast if the interval is not a number.
         */
        static std::shared_ptr<ConfigurationRule> from(const std::string &);

        /**
         * @brief Builds a configuration rule from a ptree.
         * **ConfigurationRule**
         *  UID : string
         *  IncludeFilter : regex
         *  ExcludeFilter : regex
         *  Name : string
         *  Parameters :
         *     OHPublishingParameters [ISPublishingParameters]:
         *        UID : string
         *        PublishInterval : seconds
         *        NumberOfSlots: integer 1..0xffffffff
         *        OHServer : string
         *        ROOTProvider : string
         */
        static std::shared_ptr<ConfigurationRule> from(const boost::property_tree::ptree & rule);

        /**
         * @brief Builds a configuration rule by using a configuration database.
         */
        static std::shared_ptr<ConfigurationRule> from(Configuration& conf,
                                                       const dal::ConfigurationRule * rule);

        /** @brief Returns a string representation of the rule. */
        const std::string str() const;

        /** @brief Returns the name of the rule. */
        const std::string& get_name() const { return name; }

        /** @brief Returns the filter of the rule. */
        const NameFilter& get_filter() const { return filter; }

        /** @brief Gets the publication interval specified by the rule. */
        unsigned get_interval() const { return interval; }

        /** @brief Gets the number of publication slots for this rule. */
        unsigned get_nslots() const { return nslots; }

        /** 
         * @brief Gets the name of the IS/OH server, may be empty if publisher
         * is not an OH or IS kind.
        */
        const std::string& get_server() const { return server; }

        virtual ~ConfigurationRule() {}
    protected:
        /** @brief Constructor to be used by subclasses. */
    ConfigurationRule(const std::string& name, const NameFilter& filter,
                      unsigned interval, unsigned nslots,
                      const std::string& server=std::string())
      : name(name), filter(filter), interval(interval), nslots(nslots), server(server) {}
    private:
        virtual const std::string params_str() const = 0;

        const std::string name;
        const NameFilter filter;
        const unsigned interval;
        const unsigned nslots;
        const std::string server;  // IS server name, only for OH/IS, empty for others
    };

} /* namespace monsvc */
#endif /* MONSVC_CONFIGURATIONRULES_H_ */

/* Local Variables: */
/* c-basic-offset:4 */
/* End: */
